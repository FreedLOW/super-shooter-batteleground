﻿using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] Transform groundCheck = null;  //тут хранится пустой объект (воображаемая сфера) для проверки на земле ли объект или нет

    public bool onGround = false;  //Стоит ли персонаж на подходящей поверхности (или летит/падает)

    public float groundDistance = 0.4f;  //в этой переменной хранится дистанция(радиус вооброжаемой сферы) от объекта к земле

    public LayerMask groundMask;  //тут хранится маска слоёв тех объектов на которых может находится игрок, её будет проверять groundCheck на коллизию

    public float gravity = -9.81f;  //переменная в которой хранится значение силы притяжения

    private Vector3 velocity;  //переменная для регулировки скорости силы притяжения

    public float jumpHeight = 3f;  //переменная для регулировки на какую высоту сможет прыгать объект

    public void JoystickMove(Joystick joystick, CharacterController controller, float horizontalMove, float verticalMove, float walkSpeed)
    {
        OnGroundCheck();

        horizontalMove = joystick.Horizontal;
        verticalMove = joystick.Vertical;

        Vector3 move = (transform.right * horizontalMove + transform.forward * verticalMove).normalized;

        if (joystick.Horizontal >= .5f && onGround)
            controller.Move(move * walkSpeed * Time.deltaTime);
        else if (joystick.Horizontal <= -.5f && onGround)
            controller.Move(move * walkSpeed * Time.deltaTime);
        else if (joystick.Vertical >= .5f && onGround)
            controller.Move(move * walkSpeed * Time.deltaTime);
        else if (joystick.Vertical <= -.5f && onGround)
            controller.Move(move * walkSpeed * Time.deltaTime);
        else
        {
            horizontalMove = 0.0f;
            verticalMove = 0.0f;
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }

    public void KeyBordMove(CharacterController controller, Animator animator, float walkSpeed, float runSpeed)
    {
        OnGroundCheck();

        Vector3 xAxisForce = transform.right * Input.GetAxis("Horizontal");
        Vector3 zAxisForce = transform.forward * Input.GetAxis("Vertical");

        Vector3 resultXZForce = (xAxisForce + zAxisForce).normalized;    //Складываем вектора

        if (onGround)
            controller.Move(resultXZForce * walkSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.Space) && onGround)
            Jump();  //если нажат пробел и стоял на земел, то игрок прыгает (перемещается вверх)

        if (Input.GetKey(KeyCode.LeftShift))// && onGround)
            controller.Move(resultXZForce * runSpeed * Time.deltaTime);

        velocity.y += gravity * Time.deltaTime;  //записываю переменной значение силы притяжения по оси У, т.е объект будет тянуть вниз

        controller.Move(velocity * Time.deltaTime);  //снова использую функцию Move и в ней использую velocity(скорость) для придания движения по оси У, т.е будет свободное падение
    }

    //метод в котором проверяется стоит ли на земле игрок:
    public void OnGroundCheck()
    {
        //записываю в булевую переменную значение проверки на земле ли персонаж, т.е. если groundMask в колизии то переменная будет тру, иначе фолс:
        onGround = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (onGround && velocity.y < 0)  //если на земле и скорость тяготения меньше нуля, то:
            velocity.y = -2f;  //если на земле, то скорось силы приятжения приравниваю к 2, можно было и 0 сделать, но персонаж будет легко двигаться
    }

    public void Jump()
    {
        velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);  //присваиваю скорости силы тяготения, то значение на которе может макс. высоко прыгнуть персонаж, для этого использую формулу V=sqrt(h*-2*g)
    }
}