﻿using System.Collections;
using UnityEngine;

public class AmmoController : MonoBehaviour
{
    public int maxAmmo = 240;  //максимальное колличество которое может носить игрок для оружия(у каждой пушки будет своё значение)

    public int currentMaxAmmo;  //текущее колличество патронов общее для оружия

    public int maxAmmoMagazine = 30;  //сколько патронов в одном магазине автоматического оружия

    public int currentAmmo;  //локальная переменная для перваначально запоминания колличества патронов

    public float reloadTime = 3f;  //время на затрату перезарядки оружия

    private bool isReloading = false;  //переменная для контроля состояния перезарядки

    public void GunReload()
    {
        if (isReloading)
            return;

        //для телефона:
        MobileReload();
    }

    public void MobileReload()
    {
        if (currentMaxAmmo > 0)  //если запасные магазины есть, то пережаряемся
            StartCoroutine(Reload());
        else if (currentAmmo == maxAmmoMagazine)
            Debug.Log("Why you reload bich??!");  //тут добавить звук типа перезарядка не нужна
        else if (currentMaxAmmo <= 0)  //если нет, то пезда тебе!
            Debug.Log("Find magazins bich!");  //сюда можно вставить звук типа нету магазинов запасных

        return;
    }

    private IEnumerator Reload() //создаю корутину перезарядки
    {
        isReloading = true;  //при запуске корутины переключаемся на состояние перезарядки

        yield return new WaitForSeconds(reloadTime);  //ждём время перезарядки

        currentMaxAmmo -= maxAmmoMagazine;  //уменьшаю колличество обойм

        currentAmmo = maxAmmoMagazine;  //обновляем колличество патронов в обойме, т.е. перезарядка окончена

        isReloading = false;  //в концу переключаемся в обычный режим
    }
}