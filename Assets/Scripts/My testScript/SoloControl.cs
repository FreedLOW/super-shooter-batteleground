﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloControl : MonoBehaviour
{   //основные статы :)
    public float speedMove; //скорость передвижения
    public float jumpPower; //сила прыжка

    //настройка самой девчули
    private float gravityForce;
    private Vector3 moveVector;
    
    //ссылки на модули(комп.)
    private CharacterController control;
    private Animator anim;

    private void Start()
    {
        speedMove = 5f;
        jumpPower = 20f;
        control = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        MovePlayer();//вызываем передвижение в апдейт
        GamingGravity();//гравитацию
    }

    //перемещение персонажа
    private void MovePlayer()
    {
        //Так она управляется когда прыгает - УБИРАЕМ ТАКУЮ ХНЮ
        if (control.isGrounded)
        {
            anim.ResetTrigger("Jump");
            anim.SetBool("Falling", false);

            //перемещение по поверхности
            moveVector = Vector3.zero; //обнуляем вектор перемещения
            moveVector.x = Input.GetAxis("Horizontal") * speedMove;
            moveVector.z = Input.GetAxis("Vertical") * speedMove;

            //АНИМАЦИИИИИИИИИИИИИ движения
            if (moveVector.x != 0 || moveVector.z != 0) anim.SetBool("Walk", true);
            else anim.SetBool("Walk", false);

            //двигаемся куда бежим
            if (Vector3.Angle(Vector3.forward, moveVector) > 1f || Vector3.Angle(Vector3.forward, moveVector) == 0) //поварачиваем перед гравитацией
            {
                Vector3 direct = Vector3.RotateTowards(transform.forward, moveVector, speedMove, 0.0f); //Использую вектор 3 , локальное направление, вектор , скорость поворота + МАГНИТУДА
                transform.rotation = Quaternion.LookRotation(direct);  //поворот сука
            }

        }
        else
        {
            if (gravityForce < -3f) anim.SetBool("Falling", true);
        }            

        moveVector.y = gravityForce; //действие гравитации на ось (ну думаю тут очевидно)
        control.Move(moveVector * Time.deltaTime); // метод передвижения по направлению
    }
    
    //Rigbody - нахуй нам не нужен

    private void GamingGravity()
    {   // Проверка земли и гравитации
        if (!control.isGrounded) gravityForce -= 20f * Time.deltaTime;
        else gravityForce = -1f;
        // ПРЫЫЫЫЫЫЖОК  
        if (Input.GetKeyDown(KeyCode.Space) && control.isGrounded)
        {
            gravityForce = jumpPower;
            anim.SetTrigger("Jump");


        }

    }

}
