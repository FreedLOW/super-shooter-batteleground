﻿using System.Collections;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class CameraBehaviour : FirstViewCamera
{
    void Start()
    {
        //блокирую курсор мыши при старте игры:
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        this.LateUpdateAsObservable()
            .Subscribe(p =>
            {
                CameraRotation();
            })
            .AddTo(this);
    }
}