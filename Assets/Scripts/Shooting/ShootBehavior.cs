﻿using System.Collections;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class ShootBehavior : RayShoot
{
    void Start()
    {
        currentMaxAmmo = maxAmmo;  //устанавливаю сколько максимально может носить с собой потронов игрок

        currentAmmo = maxAmmoMagazine;  //устанавливаю текущее колличество патронов

        this.UpdateAsObservable()
            .Where(m => Input.GetMouseButtonDown(0) && Time.time >= nextTimeToShoot && !Input.GetMouseButton(1))
            .Subscribe(s =>
            {
                //nextTimeToShoot = Time.time + 1f / shootRate;  //обновляю значение переменной, т.е. делаю задержку между выстрелами

                Shooting();
            })
            .AddTo(this);

        this.UpdateAsObservable()
            .Where(l => Input.GetKeyDown(KeyCode.R))
            .Subscribe(p =>
            {
                GunReload();
            })
            .AddTo(this);
    }
}