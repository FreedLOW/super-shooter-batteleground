﻿using System.Collections;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class RayShoot : AmmoController
{
    [SerializeField] Camera mainCamera = null;

    [SerializeField] ParticleSystem particleEffect = null;

    [SerializeField] float shootDistance = 100f;

    [SerializeField] float damage = 1f;  //сколь урона наносит оружие(для каждой пушки своё)

    [SerializeField] AssetReferenceGameObject[] holes = null;

    public float nextTimeToShoot = .1f;

    public float shootRate = 20f;  //переменная для регулировки скорости выстрела

    public void Shooting()
    {
        if (currentAmmo > 0)
        {
            currentAmmo--;
            nextTimeToShoot = Time.time + 1f / shootRate;  //обновляю значение переменной
            Shoot();
        }
        else if (currentAmmo == 0)
        {
            //тут можно привязать звук что игрок без патронов
            Debug.Log("Zero ammo! Reload or find ammo bich!");
        }
    }
    
    public void Shoot()
    {
        particleEffect.Play();

        RaycastHit hit;

        if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out hit, shootDistance))
        {
            Debug.Log("You shoot!");

            Target target = hit.transform.GetComponent<Target>();

            if (target != null)
                target.TakeDamage(damage);

            StartCoroutine(CheckMaterial(hit));
        }
    }

    private IEnumerator CheckMaterial(RaycastHit hit)
    {
        #region Проверка материала на который попадает луч и создание эффектов попадания
        var material = hit.collider.material.name;  //получаю имя материала с которым перессёкся луч

        if (material == "Stone (Instance)")
        {
            var holeEffects = Addressables.InstantiateAsync(holes[0], hit.point, Quaternion.LookRotation(hit.normal));  //если название совпало то создаю эффект
            yield return new WaitForSeconds(1f);  //задержка 1 секунда
            Addressables.ReleaseInstance(holeEffects);  //выгружаю из памяти префаб
        }
        else if (material == "Wood (Instance)")
        {
            var holeEffects = Addressables.InstantiateAsync(holes[1], hit.point, Quaternion.LookRotation(hit.normal));
            yield return new WaitForSeconds(1f);
            Addressables.ReleaseInstance(holeEffects);
        }
        else if (material == "Metal (Instance)")
        {
            var holeEffects = Addressables.InstantiateAsync(holes[2], hit.point, Quaternion.LookRotation(hit.normal));
            yield return new WaitForSeconds(1f);
            Addressables.ReleaseInstance(holeEffects);
        }
        else if (material == "Sand (Instance)")
        {
            var holeEffects = Addressables.InstantiateAsync(holes[3], hit.point, Quaternion.LookRotation(hit.normal));
            yield return new WaitForSeconds(1f);
            Addressables.ReleaseInstance(holeEffects);
        }
        else if (material == "Character (Instance)")
        {
            var holeEffects = Addressables.InstantiateAsync(holes[4], hit.point, Quaternion.LookRotation(hit.normal));
            yield return new WaitForSeconds(2f);
            Addressables.ReleaseInstance(holeEffects);
        }
        else if (material == "CharacterBig (Instance)")
        {
            var holeEffects = Addressables.InstantiateAsync(holes[5], hit.point, Quaternion.LookRotation(hit.normal));
            yield return new WaitForSeconds(2f);
            Addressables.ReleaseInstance(holeEffects);
        }
        #endregion
    }
}