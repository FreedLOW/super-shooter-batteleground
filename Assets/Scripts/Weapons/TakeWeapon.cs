﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeWeapon : MonoBehaviour
{
    float distanceToWeapon = 10f;

    [SerializeField] GameObject weaponPlayer = null;

    void Update()
    {
        CheckTakeWeapon();
    }

    void CheckTakeWeapon()
    {
        var playerCamera = Camera.main;

        RaycastHit hit;

        if (Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out hit, distanceToWeapon))
        {
            var weapon = hit.collider.tag;

            if (weapon == "Weapon" && Input.GetKeyDown(KeyCode.E))
            {
                //анимация подбора оружия

                //перемещать оружие с пола в определённую точку

                //а пока просто активировать
                Destroy(hit.transform.gameObject);
                weaponPlayer.SetActive(true);
            }
        }
    }
}