﻿using System.Collections;
using UnityEngine;

public class TakePatrons : MonoBehaviour
{
    float distanceToPatrons = 10f;

    [SerializeField] int countPattrons = 20;

    AmmoController ammoController = new AmmoController();  //экземпляр клсса

    ShootBehaviorPistol behaviorPistol = new ShootBehaviorPistol();

    private void Update()
    {
    }

    public void CheckTakePatrons(int currentMaxAmmo)
    {
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, distanceToPatrons))
        {
            var patron = hit.collider.tag;

            if (patron == "Patrons" && Input.GetKeyDown(KeyCode.E))
            {
                //обновлять колличество доступных патронов, т.е магазинов:
                //ammoController.maxAmmo += countPattrons;
                currentMaxAmmo += countPattrons;

                //сделать выгрузку через адресебл а пока пусть так будет
                Destroy(gameObject);
            }
        }
    }
}