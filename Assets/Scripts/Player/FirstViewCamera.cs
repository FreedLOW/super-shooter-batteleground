﻿using UnityEngine;

public class FirstViewCamera : MonoBehaviour
{
    public Transform player;

    public float turnedSpeedCameraX = 360f;  //скорось вращения камеры по оси Х

    public float turnedSpeedCameraY = 240f;  //скорось вращения камеры по оси У

    public float mobileSensitivity = 40f;  //скорость вращение камеры по осям для телефона

    float currentRotationCamX = 0;  //переменная в которой хранится текущее врщение камеры по оси Х

    float limitCamraXUp = 45f;  //переменная для ограничения поворота камеры по оси Х(вверх)

    float limitCameraXDown = 55f;  //переменная для ограничения поворота камеры по оси Х(вниз)

    public DynamicJoystick cameraJoystick;

    public void CameraRotation()
    {
        //получаю значения движения камеры по оясм:
        //мышка:
        var moveX = Input.GetAxis("Mouse X") * turnedSpeedCameraX * Time.deltaTime;
        var moveY = Input.GetAxis("Mouse Y") * turnedSpeedCameraY * Time.deltaTime;
        //джойстик для телефона:
        //var moveX = cameraJoystick.Horizontal * mobileSensitivity * Time.deltaTime;
        //var moveY = cameraJoystick.Vertical * mobileSensitivity * Time.deltaTime;

        if (moveX != 0)
        {
            player.RotateAround(player.position, Vector3.up, moveX);  //поворачиваю персонажа вслед за камерой по оси Х
        }

        if (moveY != 0)
        {
            currentRotationCamX -= moveY;  //передаю движение по оси У и инвертирую его используя -
            currentRotationCamX = Mathf.Clamp(currentRotationCamX, -limitCamraXUp, limitCameraXDown);  //ограничиваю поворот камеры по оси Х
            float rotationY = transform.localEulerAngles.y;  //локальная переменная изменения вращения камеры по оси У
            transform.localEulerAngles = new Vector3(currentRotationCamX, rotationY, 0);  //возвращаю новые значения поворота камеры по осям
        }
    }
}