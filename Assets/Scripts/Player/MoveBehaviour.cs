﻿using System.Collections;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class MoveBehaviour : Movement
{
    [SerializeField] Joystick joystick;

    float horizontalMove;

    float verticalMove;

    [SerializeField] float walkSpeed = 10.0f;

    [SerializeField] public float runSpeed = 20.0f;  //коєффициент для бега

    CharacterController controller;

    Animator animator;

    void Start()
    {
        controller = GetComponent<CharacterController>();

        animator = GetComponent<Animator>();

        this.FixedUpdateAsObservable()
            .Subscribe(h =>
            {
                //JoystickMove(joystick, controller, horizontalMove, verticalMove, walkSpeed);
                KeyBordMove(controller, animator, walkSpeed, runSpeed);
            })
            .AddTo(this);
    }
}