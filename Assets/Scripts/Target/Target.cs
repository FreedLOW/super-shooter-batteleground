﻿using UnityEngine;

public class Target : MonoBehaviour
{
    public float healthEnemy = 100f;

    public void TakeDamage(float damage)
    {
        healthEnemy -= damage;

        if (healthEnemy <= 0)
            Die();
    }

    void Die()
    {
        Destroy(gameObject);
    }
}